const jwt = require('jsonwebtoken');
const config = require('../config');
// const User = require('../models/User');

module.exports.authenticate = (req, res, next) => {
    const token = req.headers['token'];
    if (token) {
        var decoded = jwt.verify(token, config.secretKey);
        if (decoded == undefined) {
            return res.status(400).json({ res, message: 'Failed to authenticate token.' })
        }
        req.user = decoded;
        return next();
    }
    return res.status(401).json({ message: 'You must be provide token.' })
}

module.exports.authorize = (roleArray) => (req, res, next) => {
    const { role } = req.user;
    const index = roleArray.findIndex(e => e === role);

    if (index > -1) return next();

    res.status(403).json({ message: 'You are not allowed to access' })
}