const jwt = require('jsonwebtoken');
const config = require('../config')

module.exports.createToken = (user, expire) => {
    return jwt.sign(user, config.secretKey, {
        expiresIn: expire
    })
}