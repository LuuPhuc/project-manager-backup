import { getToken } from '../api/UserAPI'

export function getHeaders (token) {
    let headers  = {
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json",
    }

    token = token || getToken()
    if (token)
        headers.token = token;
    return headers
}
