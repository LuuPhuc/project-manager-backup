import React, { Component } from 'react'
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { getListUsers } from './actions/list_member'

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

class ListMember extends Component {

    constructor (props) {
        super(props)

        this.props.getListUsers()
    }

    render () {
        const { classes, memberData } = this.props;
        console.log('__', memberData);
        const { users } = memberData || {};

        return (
            <div className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>

                        <TableRow>
                            <TableCell>STT</TableCell>
                            <TableCell>Full name</TableCell>
                            <TableCell>Phone</TableCell>
                            <TableCell>Date created</TableCell>
                        </TableRow>

                    </TableHead>
                    <TableBody>
                        {
                            (users || []).map((user, i) => {
                                const { name, phone, createdAt } = user;
                                return (
                                    <TableRow key={i}>
                                        <TableCell component="th" scope="row">{i}</TableCell>
                                        <TableCell>{name || '-'}</TableCell>
                                        <TableCell>{phone || '-'}</TableCell>
                                        <TableCell>{new Date(createdAt).toDateString()}</TableCell>
                                    </TableRow>
                                )
                            })
                        }
                    </TableBody>
                </Table>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    memberData: state.memberData
})

const mapDispatchToProps = dispatch => ({
    getListUsers: () => dispatch(getListUsers()),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ListMember))