import config from '../config'
import { getHeaders } from '../utils/common'
import axios from 'axios'

export function getProjectCreated () {
    return new Promise((resolve, reject) => {
        return axios.get(config.api_url + '/projects', { headers: getHeaders() })
            .then(res => {
                resolve(res.data);
            })
            .catch(err => reject(err))
    })
}

export function getProjectAssign () {
    return new Promise((resolve, reject) => {
        return axios.get(config.api_url + '/projects/assign/user', { headers: getHeaders() })
            .then(res => {
                resolve(res.data);
            })
            .catch(err => reject(err))
    })
}

export function createProject (name) {
    return new Promise((resolve, reject) => {
        const data = {
            project: { name }
        }
        return axios.post(config.api_url + '/projects/create', data, { headers: getHeaders() })
            .then(res => {
                resolve(res.data)
            })
            .catch(err => reject(err))
    })
}

export function deleteProject (projectID) {
    return new Promise((resolve, reject) => {
        return axios.delete(config.api_url + '/projects/delete/' + projectID, {
            headers: getHeaders()
        })
            .then(res => {
                resolve(res.data)
            })
            .catch(err => reject(err))
    })
}

export function updateProject (project) {
    return new Promise((resolve, reject) => {
        const data = {
            project: { name: project.name }
        }
        return axios.put(config.api_url + '/projects/update' + project._id,
            data,
            {
                headers: getHeaders()
            })
            .then(res => {
                resolve(res.data)
            })
            .catch(err => reject(err))
    })
}

export function getProjectMember (projectID) {
    return new Promise((resolve, reject) => {
        return axios.get(config.api_url + '/projects/' + projectID + '/members', {
            headers: getHeaders()
        })
            .then(res => {
                resolve(res.data);
            })
            .catch(err => reject(err))
    })
}

export function assignMember (projectID, memberID) {
    return new Promise(async (resolve, reject) => {
        const data = {
            users: [memberID]
        }
        return axios.post(config.api_url + '/projects/' + projectID + '/assign', data, { headers: getHeaders() })
            .then(res => {
                resolve(res.data.users)
            })
            .catch(err => reject(err))
    })
}

export function unassignMember (projectID, memberID) {
    return new Promise(async (resolve, reject) => {
        const data = {
            users: [memberID]
        }
        return axios.delete(config.api_url + '/projects/delete/unassign/' + projectID, data, { headers: getHeaders() })
            .then(res => {
                resolve(res.data)
            })
            .catch(err => reject(err))
    })
}