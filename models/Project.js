const mongoose = require('mongoose');

const ProjectSchema = new mongoose.Schema({
    name:
    {
        type: String,
        trim: true
    },
    creator:
    {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    members:
        [{
            type: mongoose.Schema.Types.ObjectId, ref: 'User'
        }],
}, { timestamps: true })

const Project = mongoose.model('Project', ProjectSchema, 'Project')

module.exports = {
    ProjectSchema, Project
}
