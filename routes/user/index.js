const express = require('express');
const UserController = require('../../controller/user')
const { validateRegisterUser, validateLogin } = require("../../middlewares/validatation/user");
const { authenticate, authorize } = require('../../middlewares/auth');

const router = express.Router();

router.post('/', validateRegisterUser, UserController.register);
router.post('/login', validateLogin, UserController.login);

router.get('/', authenticate, UserController.findAll);
router.get('/:id', authenticate, UserController.findById);


router.put('/update/:id', authenticate, authorize(['admin']), UserController.update);
router.delete('/delete/:id', authenticate, authorize(['admin']), UserController.delete);

module.exports = router;