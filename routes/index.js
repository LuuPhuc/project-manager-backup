const express = require('express');
const router = express.Router();


// router.get('/', (req, res) => {
//     res.json({ message: 'Hello home page' });
// })

//User
router.use('/users', require('./user'))
router.use('/projects', require('./project'))

module.exports = router;
