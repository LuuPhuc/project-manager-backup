const express = require('express');
const ProjectController = require('../../controller/project')
const { authenticate, authorize } = require('../../middlewares/auth');

const router = express.Router();

router.post('/create', authenticate, authorize(['admin']), ProjectController.create);
router.post('/:id/assign', authenticate, authorize(['admin']), ProjectController.assign);

router.get('/:id', authenticate, authorize(['admin']), ProjectController.findById);
router.get('/', authenticate, authorize(['admin']), ProjectController.findAll);
router.get('/assign/user', authenticate, ProjectController.projectAssign);
router.get('/:id/members', authenticate, ProjectController.findMembers);


router.put('/update/:id', authenticate, ProjectController.update)
router.delete('/delete/:id', authenticate, authorize(['admin']), ProjectController.delete);
router.delete('/delete/unassign/:id', authenticate, ProjectController.unassign);

module.exports = router;