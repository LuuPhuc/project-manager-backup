const mongoose = require('mongoose');

const connectDB = async (mongoURL) => {
    try {
        const connection = await mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: true
        })
        return console.log(`MongoDB connected: ${connection.connection.host}`)
    } catch (e) {
        return Promise.reject(e)
    }
}

module.exports = connectDB;