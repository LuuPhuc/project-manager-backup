const _ = require('lodash');
const { Project } = require('../../models/Project');


/**
 * Create new project
 * @param: Information Project
 * @author: Luu Phuc
 */
module.exports.create = async (req, res, next) => {
    try {
        const p = Object.assign(req.body.project, { creator: req.user._id })
        Project.create(p);
        return res.status(200).json({ name: req.body.project.name, message: 'Create successful' })
    } catch (e) {
        return Promise.reject(e)
    }
}

/**
 * Assign project
 * @param: id
 * @author: Luu Phuc
*/
module.exports.assign = async (req, res, next) => {
    let { id } = req.params
    Project.findOne({ _id: id })
        .then(project => {
            project.members = project.members.concat(req.body.users)
            return project.save()
        })
        .then(project => {
            return res.status(200).json({ project, message: 'Assign successful' })
        })
        .catch(err => {
            return Promise.reject(err)
        })
}
/**
 * Unassign project
 * @author: Luu Phuc 
 */
module.exports.unassign = async (req, res, next) => {
    const { id } = req.params;
    try {
        const project = await Project.findById(id)
        if (!project) return Promise.reject({ message: 'Project dose not exists' })
        project.members = project.members.filter(m => !req.body.users.includes(m.toString()));
        project.save();
        return res.status(200).json({ message: 'Delete successful' })
    } catch (e) {
        return Promise.reject(e)
    }
}
/**
 * Delete project
 * @param: id
 * @author: Luu Phuc
 */

module.exports.delete = async (req, res, next) => {
    const { id } = req.params;
    Project.deleteOne({ _id: id })
        .then(() => {
            return res.status(200).json({ _id: req.params.id, message: 'Delete successful' })
        })
        .catch(err => res.json(err))
}

/**
 * Find project by id
 * @param: id
 * @author: Luu Phuc
 */
module.exports.findById = async (req, res, next) => {
    const { id } = req.params;
    try {
        const project = await Project.findById(id);
        if (!project) return res.status(400).json({ message: 'Project dose not exists' })
        return res.status(200).json(project)
    } catch (e) {
        return res.json(e)
    }
}

/**
 * Find all projects
 * @author: Luu Phuc
 */
module.exports.findAll = async (req, res, next) => {
    try {
        const allProject = await Project.find({})
        if (!allProject) return Promise.reject({ message: 'Some thing wrong' })
        return res.status(200).json(allProject)
    } catch (e) {
        return res.json(e)
    }
}

/**
 * Find project assign
 * @author: Luu Phuc
 */

module.exports.projectAssign = async (req, res, next) => {
    try {
        const projects = await Project.find({
            members: { "$all": [req.user.id] }
        })
        console.log()
        return res.status(200).json({ projects, message: 'Get successful' })
    } catch (e) {
        return Promise.reject(e)
    }
}

/**
 * Find members of Project
 * @author: Luu Phuc
 */

module.exports.findMembers = async (req, res, next) => {
    const { id } = req.params;
    try {
        const project = await Project.findById(id).populate('members')
        if (!project) return Promise.reject({ message: 'Project dose not exists' })
        return res.status(200).json({ project: project.members })
    } catch (e) {
        return Promise.reject(e)
    }
}

/**
 * Update projects
 * @author: Luu Phuc
 */

module.exports.update = async (req, res, next) => {
    let { id } = req.params
    Project.findById(id)
        .then(project => {
            if (!project) {
                return Promise.reject({ message: 'Project dose not exists' })
            }
            Object.assign(project, req.body.project)
            return project.save()
        })
        .then(project => {
            return res.status(200).json({ project, message: 'Update successful' })
        })
        .catch(err => {
            return Promise.reject(err)
        })
}